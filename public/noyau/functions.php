<?php
/*
./noyau/fonctions.php
Fonctions personalisées
 */

namespace Noyau\Fonctions;

/**
 * [tonquer description]
 * @param  string  $chaine        [description]
 * @param  integer $nbrCaracteres [description]
 * @return string                 [description]
 */
function tronquer ( string $chaine, int $nbrCaracteres = 200) : string {
  if(strlen($chaine) > $nbrCaracteres) :
    $positionEspace = strpos($chaine, ' ', $nbrCaracteres);
    return substr($chaine, 0, $positionEspace);
  endif;
  return $chaine;

}

/**
 * [dateFormat description]
 * @param  string $datedb [description]
 * @param  string $format [description]
 * @return [type]         [description]
 */
function dateFormat (string $datedb, string $format = "D, d M Y") {
  $date = date_create($datedb);
  return date_format($date, $format);
}
