<?php
/*
  ./app/routeurs/user.php
  Il existe un $_GET['users']
 */
include_once '../app/controleurs/postsControleur.php';

switch ($_GET['post']) {
  /*
  DÉTAILS D'UN POST
  PATTERN: ?post=show&id=xxx
  CTRL: postsControleur
  ACTION: showAction
*/
  case 'show':  		\App\Controleurs\PostsControleur\showAction($connexion, $_GET['id']);
    break;

    /*
    RECHERCHE D'UN POT
    PATERN : index.php?post=search
    CTRL : PostsControleur
    ACTION : search
     */
    case 'search':
		\App\Controleurs\PostsControleur\searchAction($connexion, $_POST['search']);
      break;

  default:
    // code...
    break;
}
