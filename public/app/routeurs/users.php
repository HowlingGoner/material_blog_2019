<?php
/*
  ./app/routeurs/user.php
  Il existe un $_GET['users']
 */

switch ($_GET['users']) {
  /*
  FORMULAIRE DE CONNEXION
  PATERN : index.php?users=loginForm
  CTRL : UsersControleur
  ACTION : login form
   */
  case 'loginForm':
    include_once '../app/controleurs/usersControleur.php';
    \App\Controleurs\UsersControleur\loginFormAction();
    break;

    /*
    TRAITEMENT FORMULAIRE DE CONNEXION
    PATERN : index.php?users=loginVerification
    CTRL : UsersControleur
    ACTION : login form
     */
    case 'loginVerification':
      include_once '../app/controleurs/usersControleur.php';
      \App\Controleurs\UsersControleur\loginVerificationAction($connexion, ['login' => $_POST['login'], 'pwd' => $_POST['pwd']]);
      break;

  default:
    // code...
    break;
}
