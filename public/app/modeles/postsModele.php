<?php
/*
    ./app/modeles/postsModele.php
 */
namespace App\Modeles\PostsModele;

function findAll(\PDO $connexion){
  $sql = 'SELECT *, posts.id AS PostID
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          ORDER BY datePublication DESC
          LIMIT 5;';

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findOneById(\PDO $connexion, int $id) :array {
    $sql = 'SELECT *
            FROM posts
            JOIN auteurs ON posts.auteur = auteurs.id
            WHERE posts.id = :id;';
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id, \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
}

function findAllByCategorie(\PDO $connexion, int $id) :array {
	$sql = 'SELECT *, posts.id AS PostID
          FROM posts
		  JOIN auteurs ON posts.auteur = auteurs.id
		  JOIN posts_has_categories ON posts.id = posts_has_categories.post
		  WHERE categorie = :categorie
          ORDER BY datePublication DESC
          LIMIT 5;';
	$rs = $connexion->prepare($sql);
    $rs->bindValue(':categorie', $id, \PDO::PARAM_INT);
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findAllBySearch (\PDO $connexion,  string $search) {
  $words = explode(' ', trim($search));

	$sql = "SELECT DISTINCT posts.titre AS titrePost,
					 posts.slug AS slugPost,
					 posts.id AS postId,
					 texte, media, datePublication, pseudo
		      FROM posts
		      JOIN auteurs ON auteur = auteurs.id
		      JOIN posts_has_categories ON posts.id = posts_has_categories.post
		      JOIN categories ON categorie = categories.id
		      WHERE 1 = 0 ";
  for ($i=0; $i<count($words); $i++) :
    $sql .= " OR posts.titre      LIKE :word$i
		          OR texte            LIKE :word$i
			        OR categories.titre LIKE :word$i
			        OR pseudo           LIKE :word$i ";
  endfor;
  $sql .= ';';

  $rs = $connexion->prepare($sql);
  for ($i=0; $i<count($words); $i++) :
	  $rs->bindValue(":word$i", '%' .$words[$i]. '%', \PDO::PARAM_STR);
  endfor;
  $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);

}
