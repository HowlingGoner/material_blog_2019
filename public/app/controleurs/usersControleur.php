<?php
/*
  ./app/controleurs/userssControleur.php
 */

namespace App\Controleurs\UsersControleur;
use App\Modeles\UsersModele as User;

/**
 * [loginFormAction description]
 * @return [type] [description]
 */
function loginFormAction() {
  // Je n'ai rien à aller chercher dans le modèle
  // Je charge la vue loginform dans $content1
  GLOBAL $content1;
  ob_start();
    include '../app/vues/users/loginForm.php';
  $content1 = ob_get_clean();
}
/**
 * [loginVerification description]
 * @param  PDO    $connexion [description]
 * @param  array  $data      [description]
 * @return [type]            [description]
 */
function loginVerificationAction (\PDO $connexion, array $data) {
  // Je vais chercher le user qui correspond au logi et au mot de passe
  include_once '../app/modeles/usersModele.php';
  $user = User\findOneByLoginAndPassword($connexion, $data);

  // Si j'ai trouvé un user, on le redirige vers le backoffice
  // Sinon je le redirige vers le formulaire avec un message d'erreur
  if ($user) :
    $_SESSION['user'] = $user;
    header('location:' . ROOT_BACKOFFICE);
  else:
    header('location: '. ROOT_PUBLIC . 'login?error=1');
  endif;

}
