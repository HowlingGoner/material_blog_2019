<?php
/*
  ./app/controleurs/pagesControleur.php
 */

namespace App\Controleurs\CategoriesControleur;
use App\Modeles\CategoriesModele as Categorie;

function menuAction(\PDO $connexion) {
  // Je vais chercher les données dans le modèle
  include_once '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);

  // Je charge la vue directement
  include '../app/vues/categories/menu.php';

}

function showAction(\PDO $connexion, int $id) {
	// Je demande lacategorie au modele 
	include_once '../app/modeles/categoriesModele.php';
	$categorie = Categorie\findOneById($connexion, $id);
	
	include_once '../app/modeles/postsModele.php';
	$posts = \App\Modeles\PostsModele\findAllByCategorie($connexion, $id);
	
	GLOBAL $content1, $title;
	$title = $categorie['titre'];
	ob_start();
		include_once '../app/vues/categories/show.php';
	$content1 = ob_get_clean();
}
