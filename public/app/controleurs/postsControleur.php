<?php
/*
    ./app/controleurs/postsControleur.php
 */
namespace App\Controleurs\PostsControleur;
use \App\Modeles\PostsModele AS Post;

function indexAction(\PDO $connexion) {
  include_once '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);

  GLOBAL $content1, $title;
  $title = POSTS_INDEX_TITLE;
  ob_start();
    include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();
}

function showAction(\PDO $connexion, int $id) {
  include_once '../app/modeles/postsModele.php';
  $post = Post\findOneById($connexion, $id);

  GLOBAL $content1, $title;
  $title = $post['titre'];
  ob_start();
    include '../app/vues/posts/show.php';
  $content1 = ob_get_clean();
}

function searchAction(\PDO $connexion, string $search) {
	// Je demande la liste des posts au modele
	include_once '../app/modeles/postsModele.php';
	$posts = Post\findAllBySearch($connexion, $search);
	
	// Je charge la vue search dans $content1
	GLOBAL $content1, $title;
  $title = $search;
  ob_start();
    include '../app/vues/posts/search.php';
  $content1 = ob_get_clean();
}
