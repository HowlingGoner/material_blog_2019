<?php
/*
  ./app/routeur.php
 */

/*
  DÉTAILS D'UNE CATÉGORIE
  PATTERN: ?post=show&id=xxx
  CTRL: postsControleur
  ACTION: showAction
*/
if (isset($_GET['categorie'])) :
  include_once '../app/controleurs/categoriesControleur.php';
  \App\Controleurs\CategoriesControleur\showAction($connexion, $_GET['id']);
/*
  CHARGEMENT DU ROUTEUR DES USERS
  PATERN : index.php?users=xxx
*/
elseif (isset($_GET['users'])) :
  include_once '../app/routeurs/users.php';

/*
 CHARGEMENT DU ROUTEUR DES POSTS
  PATTERN: index.php?post=xxx
*/
elseif (isset($_GET['post'])) :
	include '../app/routeurs/posts.php';
else :
/*
  ROUTE PAR DEFAUT
  PATTERN: /
  CTRL: postsControleur
  ACTION: index
*/
  include_once '../app/controleurs/postsControleur.php';
  \App\Controleurs\PostsControleur\indexAction($connexion);

endif;
