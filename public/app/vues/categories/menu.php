<?php
/*
  ./paa/vues/categories/menu.php
 */
?>

 <ul class="collection">
<?php
foreach ($categories as $categorie) : ?>
  <li>
  <a href="categorie/<?php echo $categorie['id']; ?>/<?php echo $categorie['slug'] ?>" class="collection-item"> <?php echo $categorie['titre']; ?></a>
  </li>
<?php endforeach; ?>
 </ul>
