
<?php
/*
  ./app/vues/posts/show.php
  variables disponibles :
    -$categori ARRAY[$id, $titre, $slug]
	- $posts ARRAYARRAY([$id, $titre, $slug, $texte, $auteur, $media, $datePublication])
 */
?>
<h1>Pots de la catégorie
	<small><?php echo $categorie['titre']; ?></small></h1>

<?php foreach ($posts as $post): ?>
  <!-- Article -->

  <article>
    <h2>
        <a href="post/<?php echo $post['id']; ?>/<?php echo $post['slug'] ?>"><?php echo $post['titre']; ?></a>
    </h2>
    <p class="lead">
      by <a href="#"><?php echo $post['pseudo']; ?></a>
    </p>
    <p> Posted on
      <?php echo \Noyau\Fonctions\dateFormat($post['datePublication']); ?>     </p>
    <hr>
       <div><?php
        echo \Noyau\Fonctions\tronquer($post['texte']);
       ?></div>
    <a href="post/<?php echo $post['PostID']; ?>/<?php echo $post['slug'] ?>">
      <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
    </a>
  </article>
  <hr>

  <!-- Fin d'article -->
<?php endforeach; ?>
