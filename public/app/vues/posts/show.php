<?php
/*
  ./app/vues/posts/show.php
  variables disponibles :
    -$post ARRAY[$id, $titre, $slug, $datePublication, $texte, $media, $auteur, ]
 */
?>
<h1><?php echo $post['titre']; ?></h1>
<!-- Author -->
<p class="lead">
  by <a href="#"><?php echo $post['pseudo'] ?></a>
</p>

<hr>

<!-- Date/Time -->
<p>Posted on <?php echo \Noyau\Fonctions\dateFormat($post['datePublication']) ?></p>

<hr>

<!-- Preview Image -->
<img class="img-responsive z-depth-2" src="<?php echo $post['media'] ?>" alt="">

<hr>

<!-- Post Content -->
<div><?php echo $post['texte'] ?></div>


<hr>
