<?php
/*
  ./app/vues/categories/editForm.php
 */
?>

<h1>Ajout d'une catégorie</h1>
<div>
  <a href="categories">
    Retour vers la liste des categories
  </a>
</div>

<form action="categories/<?php echo $categorie['id'] ?>/edit/update" method="post">
  <div>
    <label for="titre">Titre</label>
    <input type="text" name="titre" id="titre" value="<?php echo $categorie['titre'] ?>" />
  </div>
  <div>
    <label for="slug">Slug</label>
    <input type="text" name="slug" id="slug" value="<?php echo $categorie['slug'] ?>" />
  </div>

  <div><input type="submit" /></div>
</form>
