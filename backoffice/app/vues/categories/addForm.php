<?php
/*
  ./app/vues/categories/addForm.php
 */
?>

<h1>Ajout d'une catégorie</h1>
<div>
  <a href="categories">
    Retour vers la liste des categories
  </a>
</div>

<form action="categories/add/insert" method="post">
  <div>
    <label for="titre">Titre</label>
    <input type="text" name="titre" id="titre" />
  </div>
  <div>
    <label for="slug">Slug</label>
    <input type="text" name="slug" id="slug" />
  </div>

  <div><input type="submit" /></div>
</form>
