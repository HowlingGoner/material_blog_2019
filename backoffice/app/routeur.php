<?php
/*
  ./app/routeur.php
 */

/*
ROUTE DES CATEGORIES
 */
if (isset($_GET['categories'])) :
  include '../app/routeurs/categories.php';
/*
ROUTE DES USERS
 */
elseif (isset($_GET['users'])) :
  include '../app/routeurs/users.php';
elseif (isset($_GET['posts'])) :
  include '../app/routeurs/posts.php';
else :
/*
  ROUTE PAR DEFAUT
  PATTERN: /
  CTRL: postsControleur
  ACTION: index
*/
  include_once '../app/controleurs/usersControleur.php';
  \App\Controleurs\UsersControleur\dashboardAction($connexion);
endif;
