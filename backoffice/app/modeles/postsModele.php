<?php
/*
  ./app/modeles/postsModele.php
 */

namespace App\Modeles\PostsModele;

function findAll (\PDO $connexion) :array {
  $sql = 'SELECT *, posts.id AS postID
          FROM posts
          JOIN auteurs ON posts.auteur = auteurs.id
          ORDER BY titre ASC;';
  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
