<?php
/*
  ./app/modeles/categoriesModele.php
 */

namespace App\Modeles\CategoriesModele;

function findAll(\PDO $connexion) {
  $sql = "SELECT *
          FROM categories
          ORDER BY titre ASC;";

  $rs = $connexion->query($sql);
  return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

function findOneById(\PDO $connexion, int $id) :array {
	$sql = 'SELECT *
			FROM categories
			WHERE categories.id = :id;';
	$rs = $connexion->prepare($sql);
	$rs->bindValue(':id', $id, \PDO::PARAM_INT);
	$rs->execute();
	return $rs->fetch(\PDO::FETCH_ASSOC);
}

function insertOne(\PDO $connexion, array $data ) {
  $sql = 'INSERT INTO categories
          SET titre = :titre,
              slug  = :slug;';
  $rs = $connexion->prepare($sql);
	$rs->bindValue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindValue(':slug', $data['slug'], \PDO::PARAM_STR);
	return $rs->execute();
}

function updateOnebyId (\PDO $connexion, array $data) {
  $sql = 'UPDATE categories
          SET titre = :titre ,
              slug  = :slug
          WHERE id  = :id;';
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $data['id'], \PDO::PARAM_STR);
	$rs->bindValue(':titre', $data['titre'], \PDO::PARAM_STR);
  $rs->bindValue(':slug', $data['slug'], \PDO::PARAM_STR);
	return $rs->execute();
}

function deleteOneById (\PDO $connexion, int $id) {
  $sql = 'DELETE FROM categories
          WHERE id = :id;';
  $rs = $connexion->prepare($sql);
	$rs->bindValue(':id', $id, \PDO::PARAM_INT);
	return intval($rs->execute());
}
