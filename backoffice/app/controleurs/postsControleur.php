<?php
/*
  ./app/controleurs/postsControleur.php
 */

namespace App\Controleurs\PostsControleur;
use App\Modeles\PostsModele as Post;

function indexAction (\PDO $connexion) {
  include '../app/modeles/postsModele.php';
  $posts = Post\findAll($connexion);
  GLOBAL $content1, $title;
  $title = 'posts';
  ob_start();
    include '../app/vues/posts/index.php';
  $content1 = ob_get_clean();
}

function addFormAction($connexion) {
  include '../app/modeles/categoriesModele.php';
  $categories = \App\Modeles\CategoriesModele\findAll($connexion);
  include '../app/modeles/auteursModele.php';
  $auteurs = \App\Modeles\AuteursModele\findAll($connexion);
  GLOBAL $content1;
  $title = 'catégories';
  ob_start();
    include '../app/vues/posts/addForm.php';
  $content1 = ob_get_clean();
}
