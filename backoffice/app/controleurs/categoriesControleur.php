<?php
/*
  ./app/controleurs/categoriesControleur.php
 */

namespace App\Controleurs\CategoriesControleur;
use App\Modeles\CategoriesModele as Categorie;

function indexAction (\PDO $connexion) {
  include '../app/modeles/categoriesModele.php';
  $categories = Categorie\findAll($connexion);
  GLOBAL $content1, $title;
  $title = USERS_DASHBOARD_TITLE;
  ob_start();
    include '../app/vues/categories/index.php';
  $content1 = ob_get_clean();
}

function addFormAction() {
  GLOBAL $content1;
  $title = 'catégories';
  ob_start();
    include '../app/vues/categories/addForm.php';
  $content1 = ob_get_clean();
}

function insertAction (\PDO $connexion, array $data) {
  include '../app/modeles/categoriesModele.php';
  $response = Categorie\insertOne($connexion, $data);
  $title = 'catégories';
  if ($response == 1 ) :
    header('location: '. ROOT_BACKOFFICE . 'categories');
  else :
    echo "une erreur s'est produite";
  endif;
}

function editAction (\PDO $connexion, int $id) {
  include '../app/modeles/categoriesModele.php';
  $categorie = Categorie\findOneById($connexion, $id);
  GLOBAL $content1;
  $title = 'catégories';
  ob_start();
    include_once '../app/vues/categories/editForm.php';
  $content1 = ob_get_clean();
}

function updateAction (\PDO $connexion, array $data) {
  include '../app/modeles/categoriesModele.php';
  $response = Categorie\updateOnebyId($connexion ,$data);
  $title = 'catégories';
  if ($response == 1 ) :
    header('location: '. ROOT_BACKOFFICE . 'categories');
  else :
    echo "une erreur s'est produite";
  endif;
}

function deleteAction (\PDO $connexion, $id) {
  $sql = 'DELETE FROM posts_has_categories
          WHERE categorie = :id;';
    $rs = $connexion->prepare($sql);
  	$rs->bindValue(':id', $id, \PDO::PARAM_INT);
  	$rs->execute();
$title = 'catégories';
include '../app/modeles/categoriesModele.php';
  $response = Categorie\deleteOneById($connexion, $id);
  if ($response == 1 ) :
    header('location: '. ROOT_BACKOFFICE . 'categories');
  else :
    echo "une erreur s'est produite";
  endif;
}
