<?php
/*
  ./app/controleurs/usersControleur.php
 */

namespace App\Controleurs\UsersControleur;
use App\Modeles\UsersModele as User;

function dashboardAction(\PDO $connexion) {
  GLOBAL $content1, $title;
  $title = USERS_DASHBOARD_TITLE;
  ob_start();
    include '../app/vues/users/dashboard.php';
  $content1 = ob_get_clean();
}

function logoutAction() {
  unset($_SESSION['user']);
  header('location: '. ROOT_PUBLIC);
}
