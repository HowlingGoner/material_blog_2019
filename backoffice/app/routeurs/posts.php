<?php
/*
  ./app/routeurs/user.php
  Il existe un $_GET['users']
 */
include_once '../app/controleurs/postsControleur.php';

switch ($_GET['posts']) {
  case 'addForm' :
    \App\Controleurs\PostsControleur\addFormAction($connexion);
    break;
  default:
    \App\Controleurs\PostsControleur\indexAction($connexion);
    break;
}
