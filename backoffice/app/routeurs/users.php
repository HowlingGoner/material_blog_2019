<?php
/*
  ./app/routeurs/user.php
  Il existe un $_GET['users']
 */

switch ($_GET['users']) {
  /*
  DECONEXION
  PATERN : index.php?users=logout
  CTRL : UsersControleur
  ACTION : logoutAction
   */
  case 'logout':
    include_once '../app/controleurs/usersControleur.php';
    \App\Controleurs\UsersControleur\logoutAction();
    break;
  default:
    // code...
    break;
}
