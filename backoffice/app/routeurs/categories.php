<?php
/*
  ./app/routeurs/categories$.php
  Il existe un $_GET['categories']
 */
include_once '../app/controleurs/categoriesControleur.php';

switch ($_GET['categories']) {
  /*
  DECONEXION
  PATERN : index.php?users=logout
  CTRL : UsersControleur
  ACTION : logoutAction
   */
  case 'addForm':
    \App\Controleurs\CategoriesControleur\addFormAction();
    break;
  case  'insert':
      \App\Controleurs\CategoriesControleur\insertAction($connexion,
          $data = ['titre' => $_POST['titre'],
                   'slug' => $_POST['slug']
                  ]);
    break;
  case 'edit' :
    \App\Controleurs\CategoriesControleur\editAction($connexion, $_GET['id']);
    break;
  case 'update' :
    \App\Controleurs\CategoriesControleur\updateAction($connexion,
        $data = ['id' => $_GET['id'],
                 'titre' => $_POST['titre'],
                 'slug' => $_POST['slug']

          ]);
    break;
  case 'delete' :
      \App\Controleurs\CategoriesControleur\deleteAction($connexion, $_GET['id']);
    break;
  default:
    \App\Controleurs\CategoriesControleur\indexAction($connexion);
    break;
}
